#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <bit>

uint64_t to_binary(double x)
{
	return std::bit_cast<uint64_t>(x);
}

double from_binary(uint64_t x)
{
	return std::bit_cast<double>(x);
}

double error_ulp(double x_approx, double x_true)
{
	double absolute_error = fabs(x_approx - x_true);
	int exp;
	frexp(x_approx, &exp);
	double ulp = ldexp(1.0, exp - 52 - 1);
	return absolute_error / ulp;
}

//Newton cos e^x - 1 = 0
double iteration_cos_exp(double xn)
{
	double delta = (cos(exp(xn)) - 1) / (exp(xn) * sin(exp(xn))); 
	return xn + delta;
}

//Halley cos e^x - 1 = 0
double iteration_cos(double xn)
{
	double delta1 = (2 * sin(exp(xn)) * (cos(exp(xn)) - 1));
	double delta2 = ((2 * (- exp(xn)) * (-exp(xn)) * sin(exp(xn)) * sin(exp(xn))) - (cos(exp(xn)) - 1) * (- (exp(xn)) * sin(exp(xn))  - cos(exp(xn))));
	double delta = (delta1 / delta2);
	return xn + delta;
}

//Newton sin e^x - 1 = 0
double iteration_sin_exp(double xn)
{
	double delta = (sin(exp(xn)) - 1) / (exp(xn) * cos(exp(xn))); 
	return xn - delta;
}

//Halley sin e^x - 1 = 0
double iteration_sin(double xn)
{
	double ex = exp(xn);
	double f = sin(ex) - 1;
	double df = ex * cos(ex);
	double df2 = ex * cos(ex) - ex * ex * sin(ex);
	double delta = 2 * f * df / (2 * df * df - f * df2);
	return xn - delta;
}

double iteration_sq_newton(double xn)
{
	return 4 * sqrt(xn) - xn;
}

double iteration_sq_halley(double xn)
{
	double f = sqrt(xn) - 2;
	double df = 1.0 / (2 * sqrt(xn));
	double df2 = -1.0 / (4 * sqrt(xn * xn * xn));
	double delta = 2 * f * df / (2 * df * df - f * df2);
	return xn - delta;
}

double iteration_root_newton(double xn)
{
	return xn / 2 + 1.0 / xn;
}

double iteration_root_halley(double xn)
{
	double xn_2 = xn * xn;
	double xn_3 = xn_2 * xn;
	return (xn_3 * 2 + xn * 12) / (xn_2 * 6 + 4);
}

double solve(double x0, unsigned N, double (*iteration) (double) /*Function as parameter*/)
{
	for (unsigned i = 1; i <= N; i++ /*i = i + 1*/)
		x0 = iteration(x0);
	return x0;
}

int main()
{
	const char table_header[] = "N,Solution,Solution-hex,True-hex,Error ulp,Relative error,%%\n";
	const char table_format[] = "%u,%g,%llx,%llx,%g,%g\n";

	printf("*** Newtons cos(e**x) - 1 = 0  ***\n");
	{
		double pi = 3.1415926535897932384626433832795;
		double x = 1.7;
		double true_value = log(2 * pi); ////////// cos(e**x) = 1 <=> e**x = (2 * n* pi) <=> x = ln(2 * n * pi). 1.83788
		printf("True value: %g\n", true_value);
		printf(table_header);
		for (unsigned N = 1; N < 28; N++)
		{
			double solution = solve(x, N, iteration_cos_exp);
			printf(table_format, N, solution, to_binary(solution), to_binary(true_value), error_ulp(solution, true_value), fabs(solution - true_value) * 100.0 / true_value);
		}
	}

	printf("*** Halleys cos(e**x) - 1 = 0  ***\n");
	{
		double pi = 3.1415926535897932384626433832795;
		double x = 1.7;
		double true_value = log(2 * pi);  ////////// cos(e**x) = 1 <=> e**x = (2 * n* pi) <=> x = ln(2 * n * pi). 1.83788
		printf("True value: %g\n", true_value);
		printf(table_header);
		for (unsigned N = 1; N < 100; N++)
		{
			double solution = solve(x, N, iteration_cos);
			printf(table_format, N, solution, to_binary(solution), to_binary(true_value), error_ulp(solution, true_value), fabs(solution - true_value) * 100.0 / true_value);
		}
	}

	printf("*** Newtons sin(e**x) - 1 = 0  ***\n");
	{
		double pi = 3.1415926535897932384626433832795;
		double x = 0.5;
		double true_value = log(pi / 2); ////////// sin(e**x) = 1 <=> e**x = (pi/2) <=> x = ln(pi/2). 2.06102, 0.451583
		printf("True value: %g\n", true_value);
		printf(table_header);
		for (unsigned N = 1; N < 26; N++)
		{
			double solution = solve(x, N, iteration_sin_exp);
			printf(table_format, N, solution, to_binary(solution), to_binary(true_value), error_ulp(solution, true_value), fabs(solution - true_value) * 100.0 / true_value);
		}
	}

	printf("*** Halleys sin(e**x) - 1 = 0  ***\n");
	{
		double pi = 3.1415926535897932384626433832795;
		double x = 0.5;
		double true_value = log(pi / 2); ////////// sin(e**x) = 1 <=> e**x = (pi/2) <=> x = ln(pi/2). 2.06102, 0.451583
		printf("True value: %g\n", true_value);
		printf(table_header);
		for (unsigned N = 1; N < 26; N++) 
		{
			double solution = solve(x, N, iteration_sin);
			printf(table_format, N, solution, to_binary(solution), to_binary(true_value), error_ulp(solution, true_value), fabs(solution - true_value) * 100.0 / true_value);
		}
	}

	printf("*** Newtons sqrt(x) - 2 = 0  ***\n");
	{
		double x = 1;
		double true_value = 4;
		printf("True value: %g\n", true_value);
		printf(table_header);
		for (unsigned N = 1; N < 26; N++)
		{
			double solution = solve(x, N, iteration_sq_newton);
			printf(table_format, N, solution, to_binary(solution), to_binary(true_value), error_ulp(solution, true_value), fabs(solution - true_value) * 100.0 / true_value);
		}
	}

	printf("*** Halleys sqrt(x) - 2 = 0  ***\n");
	{
		double x = 1;
		double true_value = 4; ////////// sin(e**x) = 1 <=> e**x = (pi/2) <=> x = ln(pi/2). 2.06102, 0.451583
		printf("True value: %g\n", true_value);
		printf(table_header);
		for (unsigned N = 1; N < 26; N++)
		{
			double solution = solve(x, N, iteration_sq_halley);
			printf("%u,%g,%llx,%llx,%g,%g\n", N, solution, to_binary(solution), to_binary(true_value), error_ulp(solution, true_value), fabs(solution - true_value) * 100.0 / true_value);
		}
	}

	printf("*** Newtons x * x - 2 = 0  ***\n");
	{
		double x = 1;
		double true_value = 1.4142135623730950;
		printf("True value: %g\n", true_value);
		printf("N Solution Solution-hex True-hex Error,ulp Relative error,%%\n");
		for (unsigned N = 1; N < 26; N++)
		{
			double solution = solve(x, N, iteration_root_newton);
			printf("%u %g %llx %llx %g %g\n", N, solution, to_binary(solution), to_binary(true_value), error_ulp(solution, true_value), fabs(solution - true_value) * 100.0 / true_value);
		}
	}

	printf("*** Halleys x * x - 2 = 0  ***\n");
	{
		double x = 1;
		double true_value = 1.4142135623730950;
		printf("True value: %g\n", true_value);
		printf("N Solution Solution-hex True-hex Error,ulp Relative error,%%\n");
		for (unsigned N = 1; N < 26; N++)
		{
			double solution = solve(x, N, iteration_root_halley);
			printf("%u %g %llx %llx %g %g\n", N, solution, to_binary(solution), to_binary(true_value), error_ulp(solution, true_value), fabs(solution - true_value) * 100.0 / true_value);
		}
	}
}
